#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    // au lieu de float (plus de précision)
    long double sum = 0;
    
    for (int i = 1; i < argc; i++) { 
        // strtold pour long double
        // cf. man strtof
        sum += strtold(argv[i], NULL);
    }
    // Lf (pour long float, 2 chiffres après le point)
    printf("%.2Lf\n", sum);
}
