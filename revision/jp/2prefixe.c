#include<stdio.h>
#include<stdlib.h>

int longprefix(char *s1, char *s2) {
    int i = 0;
    //  à ce stade (i=0) 
    //  bonjour0
    //  ^---------s1
    //  bon0
    //  ^---------s2
    //  bonsoir0
    //  Tant que : 
    //  pas à la fin de s2 ET pas à la fin de s1
    //  ET les deux caractères *s1 et *s2 sont les mêmes
    //  on avance (s1 et s2)
    while (*s1 && *s2 && *s1 == *s2) {
        s1++; s2++; // ou bien dans le while : *s1++ == *s2++
        i++;
    }
    return i;
}


int main() {
    char bonjour[] = "bonjour";
    char bon[] = "bon";
    char aurevoir[] = "au revoir";
    char bonsoir[] = "bonsoir";

    printf("%s et %s : %d\n", bonjour, bon, longprefix(bonjour,bon));
    printf("%s et %s : %d\n", bon, bonjour, longprefix(bon, bonjour));
    printf("%s et %s : %d\n", bonjour, aurevoir, longprefix(bonjour, aurevoir));
    printf("%s et %s : %d\n", bon, aurevoir, longprefix(bon, aurevoir));
    printf("%s et %s : %d\n", aurevoir, bon, longprefix(aurevoir, bon));
    printf("%s et %s : %d\n", bonjour, bonsoir, longprefix(bonjour, bonsoir));
}
