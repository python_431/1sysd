# Rappel : comment créer une paire de clefs publique/privée ssh :

~~~~Bash
ssh-keygen -t rsa
~~~~

(répondez : entrée à chaque question)

copiez le contenu de `~/.ssh/id_rsa.pub` et dans gitlab/github 
ajoutez cette clef (section du profil "SSH Keys") 

~~~~Bash
$ cat ~/.ssh/id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDEVJ50WTKYT5lLrXXi4sixY17xYVCQ8bwwKZjeH0k9u7L3U/EaDQZrAMwdm9XbWmGMrRIc1PJHkxuE6fF0mpdHWUGb5well6780Ff7Oxgq+0g1US7dH4H7jYFCpoNpicwb6wfE8RCYUqNY5wK1N8raf0Ea1Z015SzUE4CQ9vvnjk4qWTH1wfGA2qg6Ty9aKppBwfZvOksF2i6q4GU1vOR2OsA5E3WMGeBrI/OWAV2RoCa....hOszvQA66D8TdErIuN5IgQgPxjZs988mAzrGLvAeOCAIc5fQCtF2r58GtoWDFDIK7x john@mycomputer 
~~~~

