#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    long double x,y;
    char op;

    if (argc != 4) {
        printf("Usage: %s x y op\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    x = strtold(argv[1], NULL);
    y = strtold(argv[2], NULL);
    op = argv[3][0];

    switch(op) {
        case '+':
            printf("%Lf\n", x + y);
            break;
        case '-':
            printf("%Lf\n", x - y);
            break;
        case '*':
            printf("%Lf\n", x * y);
            break;
        case '/':
            printf("%Lf\n", x / y);
            break;
        default:
            printf("Opération inconnue !\n");
            exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
}
