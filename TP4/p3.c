#include<stdio.h>

int main() {
    int N, n, i, s;
    int numbers[50];

    printf("Combien de nombres voulez-vous saisir (max 50) ? ");
    scanf("%d", &N); 

    printf("Merci de saisir %d nombres entiers\n", N);
    s = 0;
    for (i = 0; i < N; i++) {
        printf("nombre n°%d : ", i + 1);
        scanf("%d", &n);
        numbers[i] = n;
        s += n;
    }
   
    printf("Somme : %d\n", s);
    
    printf("Vous avez saisi :");
    for (i = 0; i < N; i++) {
        printf(" %d", numbers[i]);
    }
    printf("\n");
}
