# Examen 1SYSD - Versailles 2023

Le rendu attendu est un lien Internet vers un répertoire d'un
dépôt GIT public nommé `Examen` contenant *uniquement* des fichiers
sources C, un Makefile (un fichier de référence est fourni ici),
et un fichier `README.md` pour fournir d'éventuels commentaires
en français (éventuellement accompagné d'images).

Le Makefile doit être complété pour que la commande
`make` génère l'ensemble des exécutables pour tester votre code.

L'évaluation se base sur la conformité du code à réaliser _exactement_
l'opération demandée et la présence de code réalisant l'opération pour
la valider. 

## A.

 Écrire et tester un programme qui demande la saisie de deux nombres
 de type `float` (ou `double`, peu importe) comme 78.34
 ou 97.50 dans deux variables `prix1` et `prix2`
 et qui affiche le pourcentage d'augmentation (ou de baisse)
 entre `prix1` et `prix2` ( _50.00%_, par exemple si `prix1`
 vaut 40.00 et `prix2` vaut 60.00 et _-50.00%_ dans l'autre
 sens).

- nom du fichier source à fournir : `prices.c`
- nom du fichier binaire exécutable produit par la commande `make` : `prices`
 
## B.

1. Soit un tableau de nombres entiers comme, par exemple :

~~~~C
int tab[] = { 1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0 };
~~~~

Écrire et tester une fonction C `void small_to_zero(int *t, int n, int val)` 
qui change tous les éléments d'un tableau d'entiers de taille `n` (pas seulement
11 comme dans l'example ci-dessus) qui sont de valeur inférieure ou égale
à `val` par la valeur 0.

Votre code de test peut reprendre l'exemple du tableau ci-dessus et
afficher le tableau après appel de la fonction.

- nom du fichier source à fournir : `squash.c`
- nom du fichier binaire exécutable produit par la commande `make` : `squash`


2. Soient trois tableaux définis ainsi dans la fonction `main` :

~~~~C
double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
double T2[] = { 2.71,  2.5,  -1, 3, -7  }; 
double T3[5]; 
~~~~

(voir fichier `vectors.c` fourni qu'il s'agit de compléter)

Définir et tester une fonction C `void add_vectors(int n, double *t1, double *t2, long double *t3)`
 qui calcule et stocke dans le tableau passé en dernier argument les carrés des différences entre
 les éléments respectifs des deux premiers tableaux passés en argument. Tous ces tableaux sont
 supposés avoir pour longueur le premier paramètre passé à la fonction.

3. Définir et tester une fonction C `double norm_vector(int n, double *t)` qui renvoie la norme
 euclidienne d'un vecteur représenté par un tableau de longueur `n` (rappel : la norme euclidienne est
 la racine carrée de la somme des carrés des coordonnées ou composantes).

- nom du fichier source à fournir : `vectors.c` (point de départ fourni)
- nom du fichier binaire exécutable produit par la commande `make` : `vectors`

## C.

_Note_ : vous vous abstiendrez d'utiliser les fonctions définies dans
 `string.h`.

1. Écrire un programme qui demande la saisie d'une chaîne de caractères à
 l'utilateur, la stocke dans un tableau de `char` (prédéfini de taille,
 mettons, 50. On supposera donc que la chaîne ne fait pas plus de 49
 caractères).

  Écrire et tester une fonction `int count_char(char *string, char c)` 
  qui renvoie le nombre d'apparitions d'un caractère dans la chaîne
  (ex: pour `"to be or not to be"` et `'o'`, elle renvoie 4).

2. Écrire et tester une fonction `int count_words(char *string)`
  qui renvoie le nombre de mots dans une chaîne de caractères. On supposera
  que les mots sont séparés par seulement un caractère espace. Par exemple
  pour la chaîne `"to be or not to be"` on doit obtenir 6. 
  
  Examiner ce qui se passe si deux (ou plus) d'espaces séparent certains
  mots (`to be or  not to be` par exemple) ? Le résultat est-il correct ?

3. Dupliquer la fonction précédente sous le nom `count_words_better` et
  améloriez là pour qu'elle fonctionne correctement même si plus d'une
  espace consécutifs séparent certains mots. Testez votre fonction.

- nom du fichier source à fournir : `counts.c` 
- nom du fichier binaire exécutable produit par la commande `make` : `counts`

## D.

Partez du fichier fourni `intlist.c` qui concerne la manipulation de listes
chaînées de nombres entiers. 

1. Ajoutez à la structure `node` un champ de type pointeur vers `node`,
   nommé `prev` (de _previous_ : précédent).

   Modifiez la fonction de création de nœud et l'une des fonctions
   `append...` (au choix) pour que ce champ soient correctement positionné
   lors de l'ajout d'un élément à la fin d'une liste : ce champ doit
   pointer vers l'élément précédent s'il existe et valoir NULL si
   l'élément est en première place.

   Testez cette fonction en créant une fonction
   `void forth_and_back(head *node)`
   qui affiche les éléments de ce nouveau type de liste du premier au dernier 
   d'abord, puis du dernier au premier ensuite en profitant du champ
   `prev`. Testez votre fonction dans `main`.

- nom du fichier source à fournir : `intlist.c` (point de départ fourni)
- nom du fichier binaire exécutable produit par la commande `make` : `intlist`

2. Dans le fichier fourni `linus.c` vous trouverez deux façons d'écrire
   une fonction qui supprime un élément d'une liste simplement chaînée.

  Dans un fichier `README.md` répondez, en français, aux questions suivantes :

- Quelles sont les étapes nécessaires pour supprimer un élément d'une
  liste chaînée en connaissant son adresse (un pointeur vers le
  nœud concerné) ?
- Deux fonctions réalisent cette opération dans le fichier `linus.c`.
  Quelles sont les différences notables entre ces deux fonctions ?
- Pourquoi la seconde a besoin de renvoyer une valeur et l'autre non ?
  Quel est le rôle d'un pointeur vers un pointeur ici ?
- Pourquoi la seconde reçoit un pointeur vers un pointeur comme argument ?
- En quelques mots, laquelle est la plus élégante selon vous ?

(si vous souhaitez adjoindre un dessin n'hésitez pas)
 
  _Dans un entretien visible sur YouTube, Linus Torvalds, créateur du
   noyau Linux et du logiciel GIT, montre brièvement
   cet exemple pour illustrer que deux codes équivalents en terme de
   fonctionnalités peuvent être l'un élégant, l'autre non :_

   https://www.ted.com/talks/linus_torvalds_the_mind_behind_linux?language=fr&subtitle=fr (vers 14mn)

