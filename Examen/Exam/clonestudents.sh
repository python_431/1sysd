#!/usr/bin/env bash

rm -rf *_*

for line in $( cat students | sed -e 's/ *$// -e 's/ /_/g' -e 's/_:_/=/' )
do 
	dir=$( echo $line | cut -d'=' -f1 )
	echo "Student: ${dir}..."
	mkdir $dir
	cd $dir
	if yes '' | git clone $( echo $line | cut -d'=' -f2 ) > /dev/null 2>&1
	then
		echo "$dir OK!"
	else
		echo "$dir: CANNOT ACCESS REPOSITORY!"
	fi
	#echo "Date $( git log -1 --format=%cd )"
	rm -rf */.git
	cd ..
done

