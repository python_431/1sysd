#!/usr/bin/env python3

from random import randint

print('Essayez de deviner un nombre entre 1 et 100 !')

secret = randint(1, 100)
n = 0

notfound =  True
while notfound:
    guess = int(input('Votre idée: '))
    n += 1
    if guess == secret:
        notfound = False
    if guess > secret:
        print('Trop grand...')
    else:
        print('Trop petit...')


print('Gagné ! En', n, 'coups !')
print('Bravo !')
