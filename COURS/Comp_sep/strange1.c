#include<stdlib.h>

// code des fonctions ici

int slen(char *s) {
    int len = 0;
    while (*s++) {
        len++;
    }
    return len;
}

int slen2(char *s) {
    int i = 0;
    for (i = 0; s[i] != 0; i++) ; // rien à faire dans le corps
    return i;
}


// maj 65-90 min 97-122
int is_upper(char *s) {
    int upper = -1;
    // on passe à faux dès qu'on trouve une minuscule
    // du coup une chaîne sans lettre du tout est 
    // considérée comme toute en majuscule, ça se
    // défend (différent de isupper() en Python)
    while (*s) {
        if (*s > 96 && *s < 123) { // minuscule
            upper = 0;
            break;
        } 
        s++;
    }
    return upper;
}

int is_lower(char *s) {
    int lower = -1;
    // on passe à faux dès qu'on trouve une majuscule
    // du coup une chaîne sans lettre du tout est 
    // considérée comme toute en minuscule, ça se
    // défend (différent de islower() en Python)
    while (*s) {
        if (*s > 64 && *s < 91) { // majuscule
            lower = 0;
            break;
        } 
        s++;
    }
    return lower;
}

void supper(char *s) {
    while (*s) {
        if (*s > 96 && *s < 123) { // minuscule
            *s -= 32;
        }
        s++;
    }
}

void slower(char *s) {
    // similaire au while ci-dessus
    for ( ; *s; s++) {
        if (*s > 64 && *s < 91) { // majuscule 
            *s += 32;
        }
    }
}
