#include<stdio.h>
#include<stdlib.h>

float augmente(float prix, float pourcentage) {
    return prix + prix*pourcentage/100;
}

float diminue(float prix, float pourcentage) {
    return prix - prix*pourcentage/100;
}

int main() {
    char action;
    float prix, pourcentage;

    printf("Augmentation (+) ou diminution (-) ? ");
    scanf("%c", &action);
    printf("Prix d'origine : ");
    scanf("%f", &prix);
    printf("Pourcentage : ");
    scanf("%f", &pourcentage);

    switch(action) {
        case '+': 
            prix = augmente(prix, pourcentage);
            printf("Nouveau prix : %.2f\n", prix);
            break;
        case '-':
            prix = diminue(prix, pourcentage);
            printf("Nouveau prix : %.2f\n", prix);
            break;
        default:
            printf("Action inconnue : %c\n", action); 
    }
}

