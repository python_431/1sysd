#include<stdio.h>
#include<stdlib.h>

typedef struct node node;
struct node {
   int val;
   node *next;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juset walk 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);

    if (head == NULL) {
        head = newnode;
    } else {
        walk = head;
        while (walk->next != NULL) {
            walk = walk->next;
        }
        walk->next = newnode;
    }
    return head;
}

void append_val2(node **phead, int val) {
    node *newnode, **walk;
    newnode = create_node(val);

    walk = phead; 
    while (*walk != NULL) {
        walk = &((*walk)->next);
    }
    *walk = newnode;
}


// Calcul du max
// rappel avec un tableau de int
// maxi = t[0];
// for (i=1; i<N; i++) {
//     if (t[i] > maxi) {
//         maxi = t[i];
//     }
// } // ici max est bien la valeur max du tableau
// avec une liste chaînée :
// (de la même façon pour min, la somme de valeurs)
int intmax(node *head) {
    node *walk;
    int maxi;
    maxi = head->val;
    walk = head->next;
    while (walk != NULL) {
        if (walk->val > maxi) {
            maxi = walk->val;
        }
        walk = walk->next;
    }
    return maxi;
} 

int main() {
    node *head = NULL;

    head = append_val(head, 42);
    head = append_val(head, 12);
    head = append_val(head, -5);
    head = append_val(head, 45);
    append_val2(&head, 11);
    append_val2(&head, 12);
    append_val2(&head, 13);
    print_list(head);
    printf("Max : %d\n", intmax(head));
}
