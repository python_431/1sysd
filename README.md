# TP du cours 1SYSD : Programmation système en C

## TP1

Deux exemples de code _« Hello World ! »_, premier exemple de _Makefile_,
prise en main de `git` :

Commandes git initiales :

- `git config --global -e` : configure nom et e-mail
- `git clone ...` : création d'un dépôt à partir d'une origine gitlab ou github
- `git init .` : initialise le répertoire courant comme dépôt (crée et
   initialise le répertoire `.git`)

Commandes git au jour le jour :

- `git add fichier ...` : ajoute le suivi d'un ou plusieurs fichiers
- `git commit -a -m "description des modifs` : enregistre l'état courant
  des fichiers suivis
- `git push` : envoie l'historique à jour sur gitlab ou github
- `git pull` : récupère l'état du dépôt sur l'origine gitlab ou github

Et pour les manipulations des fichiers suivis : ne pas utiser les 
commandes UNIX habituelles (`mv`, `rm`, etc.) mais les commandes
GIT de même nom : `git mv heLLO.c hello.c` 
