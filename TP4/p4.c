#include<stdio.h>

int main() {
    int N, n, i, s;
    int numbers[50];
    int min, max;

    printf("Combien de nombres voulez-vous saisir (max 50) ? ");
    scanf("%d", &N); 

    printf("Merci de saisir %d nombres entiers\n", N);
    s = 0;
    for (i = 0; i < N; i++) {
        printf("nombre n°%d : ", i + 1);
        scanf("%d", &n);
        numbers[i] = n;
        s += n;
    }
   
    printf("Somme : %d\n", s);
    
    printf("Vous avez saisi :");
    for (i = 0; i < N; i++) {
        printf(" %d", numbers[i]);
    }
    printf("\n");

    /* Calcul min et max */
    min = max = numbers[0];
    for (i = 1; i < N; i++) {
        if (numbers[i] < min) {
            min = numbers[i];
        }
        if (numbers[i] > max) {
            max = numbers[i];
        }
    }
    printf("Minimum : %d\n", min);
    printf("Maximum : %d\n", max);
}
