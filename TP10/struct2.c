#include<stdio.h>
#include<stdlib.h>
#include<math.h>

// même chose que struct1 mais avec malloc
// au lieu de réserver une taille max de 50

typedef struct point point;
struct point {
	long double x,y;
};

void affiche_point(point p) {
	printf("(%.2Lf, %.2Lf)\n", p.x, p.y);
}

// à compléter : calcul de longueur du vecteur (O->p)
// (hint : racine carrée (sqrtl) de la somme des carrés des coordonnées
// man sqrt pour les détails

long double longueur(point p) {
    return sqrtl(p.x*p.x + p.y*p.y); 
}

long double distance(point p1, point p2) {
    // on aurait aussi pu définir le vecteur p1p2 et utiliser la fonction
    // longeur
    return sqrtl( (p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y) );

}

void affiche_tabpoints(point *tab, int taille) {
	for (int i = 0; i < taille; i++) {
		affiche_point(tab[i]);
	}
}

void lire_point(point *p) {
	printf("x y = ");
	// scanf("%Lf %Lf", &((*p).x), &((*p).y));
	// ou encore ceci, "." a une priorité plus forte que &
	// scanf("%Lf %Lf", &(*p).x, &(*p).y);
	// ou encore ceci : p->x est une abréviation pour (*p).x
	scanf("%Lf %Lf", &p->x, &p->y);
}

int main() {
	point p1 = { 1.0, 2.0 };
	point p2,p3;
	point tab[] = { { 1.0, 2.0 }, { -1.0, 2.0 }, { 3, 1 } };
        point *tab2;
	int n;

	p2.x = -1.0;
	p2.y = 2.0;

	affiche_point(p1);
	affiche_point(p2);
	printf("Distances à l'origine : %.2Lf %.2Lf\n", longueur(p1), longueur(p2));
	printf("Distance entre p1 et p2 : %.2Lf\n", distance(p1, p2));
	affiche_tabpoints(tab, 3);

	lire_point(&p3);
	affiche_point(p3);

	printf("Combien de points souhaitez vous fournir : ");
	scanf("%d", &n);
	// allocation dynamique de mémoire
	if ( (tab2 = malloc(n*sizeof(point))) == NULL ) {
		printf("Erreur d'allocation mémoire !\n");
		exit(1);
	}
	for (int i = 0;  i < n; i++) {
		lire_point(&tab2[i]);
	}
	printf("Voici les points saisis :\n");
	affiche_tabpoints(tab2, n);
	// libération de la mémoire 
	// (ici inutile puisque le programme se termine
	// juste après... mais important si le programme
	// continuait à tourner et n'avait plus besoin de
	// tab2, sinon : fuite de mémoire ...)
	free(tab2);

	return 0;
}

