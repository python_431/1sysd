#include<stdio.h>
#include<stdlib.h>
#include<math.h>

typedef struct node node;
struct node {
   long double x,y;
   node *next;
};

node *create_node(long double x, long double y) {
    node *p;
    p = malloc(sizeof(node));
    p->x = x;
    p->y = y;
    p->next = NULL;
    return p;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("(%.2Lf, %.2Lf) ", walk->x, walk->y);
        walk = walk->next;
    }
    printf("\n");
}

node *append_val(node *head, long double x, long double y) {
    node *newnode, *walk;

    newnode = create_node(x, y);

    if (head == NULL) { // liste vide
        head = newnode;
    } else {            // on parcourt la liste jusqu'à la fin
        walk = head;
        while (walk->next != NULL) { // on va jusqu'au dernier nœud
            walk = walk->next;
        }
        walk->next = newnode; // on ajoute le nouvel élément
    }
    return head;
}

void append_node(node **phead, node *newnode) {
    node **walk;

    walk = phead;
    while (*walk != NULL) {
        walk = &((*walk)->next);
    }
    *walk = newnode;
}

void append_val2(node **phead, long double x, long double y) {
    node *newnode, **walk;
    newnode = create_node(x, y);

    walk = phead;
    while (*walk != NULL) {
        walk = &((*walk)->next);
    }
    *walk = newnode;
}

void affiche_point(node *p) {
	printf("(%.2Lf, %.2Lf)\n", p->x, p->y);
}

long double longueur(node *p) {
    return sqrtl(p->x*p->x + p->y*p->y); 
}

long double distance(node *p1, node *p2) {
    // on aurait aussi pu définir le vecteur p1p2 et utiliser la fonction
    // longeur
    return sqrtl( (p2->x - p1->x)*(p2->x - p1->x) + (p2->y - p1->y)*(p2->y - p1->y) );
}


node *lire_point() {
	node *newnode;
	long double x,y;

	printf("x y = ");
	scanf("%Lf %Lf", &x, &y);
        newnode = create_node(x, y);
	return newnode;
}

// même astuce que append_val2
// pour ne avoir un cas particulier sur
// une liste vide
int longueur_liste(node **phead) {
	node **walk;
	int i = 0;
	walk = phead;
	while (*walk != NULL) {
               walk = &((*walk)->next);
	       i++;
	}
	return i;
}

// TODO: corriger le bug
int supprime_premval(node **phead, long double x, long double y) {
	node **walk = phead;
	node **prev = phead;
	int res = 0;
	while (*walk != NULL) {
		printf("A : \n");
		print_list(*phead);
		if ( (*walk)->x == x && (*walk)->y == y ) {
			printf("%d\n", *prev == *walk);
			printf("%Lf\n", (*walk)->x);
			printf("Trouvé : \n");
			print_list(*phead);
			*prev = (*walk)->next;
			affiche_point(*walk);
			printf("%Lf\n", (*walk)->x);
			free(*walk);
			affiche_point(*walk);
			printf("Trouvé : \n");
			print_list(*phead);
			res = -1;
			break;
		}
		walk = &((*walk)->next);
                *prev = *walk;
		printf("C : \n");
		print_list(*phead);
        }
	return res;
}

int main() {
    node *head = NULL, *point1, *point2;
    int res;

    printf("longueur de liste vide : %d\n", longueur_liste(&head));

    //head = append_val(head, 42);
    //head = append_val(head, 12);
    //head = append_val(head, -5);
    //head = append_val(head, 41);
    append_val2(&head, 1.0, 2.0);
    append_val2(&head, 3.0, 1.0);
    append_val2(&head, 1.5, 1.5);
    append_val2(&head, 0.0, 4.25);
    append_val2(&head, 1.0, 8.25);
    append_val2(&head, 2.0, 7.25);
    
    print_list(head);
    //point1 = lire_point();
    //point2 = lire_point();
    //append_node(&head, point1); 
    //append_node(&head, point2); 
    print_list(head);
    //printf("d = %Lf\n", distance(point1, point2));
    printf("longueur de liste : %d\n", longueur_liste(&head));
    res = supprime_premval(&head, 0.0, 4.25);
    printf("Suppression de ??? : %d\n", res);
    print_list(head);
    printf("longueur de liste : %d\n", longueur_liste(&head));

    return 0;
}
