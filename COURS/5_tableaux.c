#include<stdio.h>

int main() {
    int numbers[] = { 42, 12, 1, 8, 12, -2, 4, 0, 11 };
    int squares[9];
    int i, s = 0;

    for (i = 0; i < 9; i++) {
        printf("%d ", numbers[i]);
        squares[i] = numbers[i] * numbers[i];
        s += numbers[i];
    }
    printf("\nSomme = %d\n", s);
    printf("Carrés : ");
    for (i = 0; i < 9; i++) {
        printf("%d ", squares[i]);
    }
    printf("\n");
}
